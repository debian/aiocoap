Source: aiocoap
Section: python
Priority: optional
Maintainer: Mazen Neifer <mazen@debian.org>
Build-Depends: debhelper (>= 11),
 dh-python,
 openssl,
 pybuild-plugin-pyproject,
 python3-all,
 python3-pytest,
 python3-setuptools,
 python3-sphinx,
 help2man,
Standards-Version: 4.7.0
Homepage: https://github.com/chrysn/aiocoap
Vcs-Git: https://salsa.debian.org/debian/aiocoap.git
Vcs-Browser: https://salsa.debian.org/debian/aiocoap

Package: python3-aiocoap
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Recommends:
 oscore,
 prettyprint,
 ws,
Suggests:
 python-aiocoap-doc,
 tinydtls,
Description: Python implementation of CoAP
 The aiocoap package is a Python implementation of CoAP, the Constrained
 Application Protocol (RFC 7252, more info at http://coap.technology/).
 .
 It uses the asyncio module introduced in Python 3.4 to facilitate concurrent
 operations while maintaining a simple to use interface and not depending on
 anything outside the standard library.

Package: python-aiocoap-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Recommends: python3-aiocoap
Description: Python implementation of CoAP (doc)
 The aiocoap package is a Python implementation of CoAP, the Constrained
 Application Protocol (RFC 7252, more info at http://coap.technology/).
 .
 It uses the asyncio module introduced in Python 3.4 to facilitate concurrent
 operations while maintaining a simple to use interface and not depending on
 anything outside the standard library.
 .
 This package contains API documentation and examples.
